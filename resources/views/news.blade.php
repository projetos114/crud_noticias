@extends('layouts.app', ['pageSlug' => 'news'])

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <form action="{{route('news')}}" method="GET">
                        @csrf
                        <div class="row">
                            <div class="col-4">
                                <input type="text" id="search" name="search" class="form-control"
                                    placeholder="Procure uma notícia">
                            </div>
                            <div class="col-4 ">
                                <button class="btn btn-sm btn-primary">Buscar</button>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="card-header">
                    <div class="row">
                        <div class="col-8">
                            <h4 class="card-title">Minhas Notícias</h4>
                        </div>
                        <div class="col-4 text-right">
                            <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add" style="color: #fff">Add
                                Notícia</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Título</th>
                                    <th scope="col">Descrição</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($noticias as $noticia)
                                    <tr>
                                        <td>{{ $noticia->id }}</td>
                                        <td>{{ $noticia->title }} </td>
                                        <td>{{ $noticia->description }} </td>
                                        <td class="td-actions">
                                            <button type="button" title="Editar" rel="tooltip" class="btn btn-link"
                                                data-toggle="modal" data-target="#edit{{ $noticia->id }}">
                                                <i class="tim-icons icon-pencil"></i>
                                            </button>
                                            <!-- Modal edit noticia-->
                                            <div class="modal fade" id="edit{{ $noticia->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Editar Notícia
                                                                {{ $noticia->title }}
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST"
                                                            action="{{ route('update.news', $noticia->id) }}">
                                                            @csrf
                                                            <div class="modal-body">

                                                                <div class="card-body">
                                                                    <div class="form-group">
                                                                        <label>Título</label>
                                                                        <input type="text" name="title"
                                                                            class="form-control"
                                                                            value="{{ $noticia->title }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Decrição</label>
                                                                        <input type="text" name="description"
                                                                            class="form-control"
                                                                            value="{{ $noticia->description }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Fechar</button>
                                                                <button type="submit"
                                                                    class="btn btn-primary">Salvar</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>



                                            <button type="button" title="Excluir" rel="tooltip" class="btn btn-link"
                                                data-toggle="modal" data-target="#excluir{{ $noticia->id }}">
                                                <i class="tim-icons icon-simple-delete"></i>
                                            </button>
                                            <!-- Modal excluir noticia-->
                                            <div class="modal fade" id="excluir{{ $noticia->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Excluir Notícia
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form method="post" autocomplete="off">
                                                                <h4 style="color: red">Certeza que deseja excluir sua
                                                                    notícia?
                                                                </h4>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Fechar</button>
                                                            <form action="{{ route('destroy.news', $noticia->id) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" title="Excluir" rel="tooltip"
                                                                    class="btn btn-primary">
                                                                    Excluir
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('white') }}/js/plugins/chartjs.min.js"></script>
    <script>
        $(document).ready(function() {
            demo.initDashboardPageCharts();
        });
    </script>
@endpush


<!-- Modal add noticia-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Adicionar Nova Notícia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('store.news') }}">
                @csrf
                <div class="modal-body">

                    <div class="card-body">
                        <div class="form-group">
                            <input type="hidden" name="users_id" value="{{ Auth::user()->id }}"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Título</label>
                            <input type="text" name="title" class="form-control" placeholder="Título">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Decrição</label>
                            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>

        </div>
    </div>
</div>
