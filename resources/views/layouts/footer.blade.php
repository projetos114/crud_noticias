<footer class="footer">
    <div class="container-fluid">
        <div class="copyright">
            &copy; {{ now()->year }} {{ _('Maria Eduarda') }} <i class="tim-icons icon-heart-2"></i>
        </div>
    </div>
</footer>
