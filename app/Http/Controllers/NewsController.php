<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Return_;
use App\Models\User;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Support\Facades\Auth;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user()->id;
        
        if($request->input('search')){
            $noticias = News::where('title', "like", "%{$request->input('search')}%")
            ->where('users_id', $user)
            ->paginate(15);
        }else{
            $noticias = News::where('users_id', $user)->paginate(15);
        }
        return view('news', ['noticias' => $noticias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("create.news");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        try {
            $noticias = News::create([
                'users_id' => $data['users_id'],
                'title' => $data['title'],
                'description' => $data['description']
            ]);
            return redirect()->route('news')->with('success', 'Notícia criada');
        } catch (\Throwable $th) {
            dd("Erro");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $noticias = News::find($id)->update([
            'title' => $data['title'],
            'description' => $data['description']
        ]);
        return redirect()->route('news')->with('success', 'Notícia editada com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $noticias = News::destroy($id);

        return redirect()->route('news')->with('success', 'Notícia excluida');
    }
}
